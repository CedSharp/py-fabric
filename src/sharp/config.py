import json
from rich import print
from os import path
from logger import Logger

logger = Logger('Config')

class Config:
    def __init__(self, root: str):
        self.root = root
        
        try:
            with open(path.join(root, 'src/main/resources/fabric.mod.json'), 'r') as f:
                config = json.load(f)
        except Exception as e:
            logger.error('Failed to load config!')
            print(e)
            exit(1)

        # Basic mod information
        self.mod_id: str = config.get('id')
        self.mod_name: str = config.get('name')
        self.entrypoint: str = config.get('entrypoints').get('main')[0]
        self.entry_file = path.join(root, self.entrypoint.replace('.', '/') + '.java')

        # Get root of mod package
        parts = self.entrypoint.split('.')
        self.entry_filename = parts.pop()
        self.package = '.'.join(parts)
        self.package_path = path.join(root, 'src/main/java', '/'.join(parts))

    def get_block_registry(self) -> str:
        return path.join(self.package_path, f'registry/{self.entry_filename}Blocks.java')

    def get_item_registry(self) -> str:
        return path.join(self.package_path, f'registry/{self.entry_filename}Items.java')

    def get_item_group_registry(self) -> str:
        return path.join(self.package_path, f'registry/{self.entry_filename}ItemGroups.java')

    def __repr__(self):
        info = 'Config {\n'
        for prop in ('mod_id', 'mod_name', 'entrypoint', 'entry_file', 'entry_filename', 'package_root'):
            info += '  {}: "{}"\n'.format(prop, getattr(self, prop))
        return info + '}'
