from os import path
from config import Config
from logger import Logger
from utils import make_id
from templates.registry import Registry

logger = Logger('ItemGroupRegistry')

template = """package [[PACKAGE]].registry;

import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

import [[PACKAGE]].[[ENTRY_CLASS]];

public class [[ENTRY_CLASS]]ItemGroups {
    public static final ItemGroup [[GROUP_NAME]] = FabricItemGroupBuilder.build(
        new Identifier([[ENTRY_CLASS]].MODID, "[[GROUP_ID]]"),
        // TODO: Change ItemStack to something that represents your mod
        () -> new ItemStack(Item.BLOCK_ITEMS.get(Blocks.COBBLESTONE)));

    public static void registerAllItemGroups() {
        [[ENTRY_CLASS]].LOGGER.info("Registered all ItemGroups!");
    }
}
"""

class ItemGroupRegistry(Registry):
    def __init__(self, config: Config):
        filepath = config.get_item_group_registry()
        super().__init__(filepath, config, logger, template)

    def get_class(self) -> str:
        self.ensure_exists()
        return self.config.entry_filename + 'ItemGroups'

    def get_group(self) -> str:
        self.ensure_exists()
        return make_id(self.config.mod_name).upper()

    def process_template(self, template: str) -> str:
        template_package = self.config.package
        entry_class = self.config.entry_filename
        group_id = make_id(self.config.mod_name)

        return template\
            .replace('[[PACKAGE]]', template_package)\
            .replace('[[ENTRY_CLASS]]', entry_class)\
            .replace('[[GROUP_NAME]]', group_id.upper())\
            .replace('[[GROUP_ID]]', group_id)

    def after_creating_file(self, file: str):
        cls_name = self.config.entry_filename + 'ItemGroups'
        rel = path.relpath(file, self.config.package_path)
        logger.info(f'Created class [white bold]{cls_name}[/] in [blue]{rel}')