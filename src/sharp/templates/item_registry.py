from os import path
from typing import List
from rich.syntax import Syntax

from config import Config
from logger import Logger
from templates.registry import Registry
from templates.item_group_registry import ItemGroupRegistry

logger = Logger('ItemRegistry')

template = """package [[PACKAGE]].registry;

import net.minecraft.item.Item;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;

import [[PACKAGE]].[[ENTRY_CLASS]];

public class [[ENTRY_CLASS]]Items {

    public static void registerAllItems() {
        SharpTools.LOGGER.info("Registered all items!");
    }

    private static Item registerItem(String name, Item item) {
        Identifier item_id = new Identifier([[ENTRY_CLASS]].MODID, name);
        [[ENTRY_CLASS]].LOGGER.info("- Register item " + item_id);
        return Registry.register(Registry.ITEM, item_id, item);
    }

    private static BlockItem registerBlockItem(String name, Block block) {
        Identifier item_id = new Identifier([[ENTRY_CLASS]].MODID, name);
        [[ENTRY_CLASS]].LOGGER.info("- Register block-item " + item_id);
        BlockItem item = new BlockItem(block, new FabricItemSettings().group([[ITEM_GROUPS]].[[ITEM_GROUP]]));
        return Registry.register(Registry.ITEM, item_id, item);
    }
}
"""

item_def = """\n\tpublic static final Item [[DEF_NAME]] =
\t\tregisterItem("[[ITEM_NAME]]", new Item(
\t\t\tnew Item.Settings().group([[ITEM_GROUPS]].[[ITEM_GROUP]])
\t\t));\n"""

item_block_def = """\n\tpublic static final Item [[DEF_NAME]] =
\t\tregisterBlockItem("[[BLOCK_ID]]", [[ENTRY_CLASS]]Blocks.[[BLOCK_NAME]]);\n"""

class ItemRegistry(Registry):
    def __init__(self, config: Config):
        filepath = config.get_item_registry()
        super().__init__(filepath, config, logger, template)

    def process_template(self, template: str) -> str:
        template_package = self.config.package
        entry_class = self.config.entry_filename
        item_group = ItemGroupRegistry(self.config)

        return template\
            .replace('[[PACKAGE]]', template_package)\
            .replace('[[ENTRY_CLASS]]', entry_class)\
            .replace('[[ITEM_GROUPS]]', item_group.get_class())\
            .replace('[[ITEM_GROUP]]', item_group.get_group())

    def add_item(self, id: str):
        self.ensure_exists()

        with open(self.path, 'r') as f:
            content = f.readlines()

        item_group = ItemGroupRegistry(self.config)
        insertDefIndex = self._get_index_to_insert_item_def(content)
        content.insert(insertDefIndex, item_def\
            .replace('[[DEF_NAME]]', id.upper())\
            .replace('[[ITEM_NAME]]', id)\
            .replace('[[ITEM_GROUPS]]', item_group.get_class())\
            .replace('[[ITEM_GROUP]]', item_group.get_group())
        )

        with open(self.path, 'w') as f:
            f.writelines(content)

        rel = path.relpath(self.path, self.config.package_path)
        logger.info(f'Added [white bold]{id.upper()}[/] to [blue]{rel}')

    def add_block_item(self, id: str):
        self.ensure_exists()

        with open(self.path, 'r') as f:
            content = f.readlines()

        insertDefIndex = self._get_index_to_insert_item_def(content)
        content.insert(insertDefIndex, item_block_def\
            .replace('[[DEF_NAME]]', id.upper())\
            .replace('[[ENTRY_CLASS]]', self.config.entry_filename)\
            .replace('[[BLOCK_ID]]', id)\
            .replace('[[BLOCK_NAME]]', id.upper())
        )

        with open(self.path, 'w') as f:
            f.writelines(content)

        rel = path.relpath(self.path, self.config.package_path)
        logger.info(f'Added [white bold]{id.upper()}[/] to [blue]{rel}')

    def after_creating_file(self, file: str):
        cls_name = self.config.entry_filename + 'Blocks'
        rel = path.relpath(file, self.config.package_path)
        logger.info(f'Created class [white bold]{cls_name}[reset] in [blue]{rel}')

    def _get_index_to_insert_item_def(self, content: str) -> int:
        registryClass = self.config.entry_filename + 'Items'

        for index, line in enumerate(content):
            if line.startswith(f'public class {registryClass} {{'):
                return index + 1

        logger.error('File content doesn\'t seem valid!')
        exit(1)
