from __future__ import annotations
from typing import List, TypedDict
from args.option import Option, OptionType
from rich import print

class Argument(TypedDict):
    name: str
    help: str

class Command:
    def __init__(self, parser, name: str, help: str = None, usage: str = None):
        from .builder import ArgsBuilder
        self.parser: ArgsBuilder = parser
        self.name = name
        self.help = help or ''
        self.usage = usage
        self.options: List[Option] = []
        self.arguments: List[str] = []

        self.flag('-h', '--help', help='Show this help message')

    def parse(self, *args, **kwargs):
        return self.parser.parse(*args, **kwargs)

    def command(self, name: str, *args, **kwargs) -> Command:
        return self.parser.command(name, *args, **kwargs)

    def flag(self, *names, **kwargs) -> Command:
        self.options.append(Option(*names, parent=self, optional=True, **kwargs))
        return self

    def option(self, *names, **kwargs) -> Command:
        self.options.append(Option(*names, parent=self, type=OptionType.STRING, **kwargs))
        return self

    def argument(self, name: str, help: str = '') -> Command:
        self.arguments.append({'name': name, 'help': help})
        return self

    def get_usage(self, both=False) -> str:
        return getattr(self, 'usage', self._generate_usage(both))

    def _generate_usage(self, both=False) -> str:
        usage = f'[bold yellow]{self.name}[/]'
        options = ''
        has_optional = False
        for option in self.options:
            if option.optional == False:
                options += ' ' + option.get_usage(both)
            else: has_optional = True
        if has_optional:
            usage += ' \[[white bold]Options[/]]'
        if len(self.arguments) > 0:
            for arg in self.arguments:
                usage += ' ' + arg['name']
        self.usage = usage + options
        return self.usage

    def __str__(self) -> str:
        return self.__repl__()

    def __repl__(self) -> str:
        return self.get_usage()