from typing import Dict, List
from rich import print
from utils import find_root
from commands import make_block, make_item
from args import ArgsBuilder
from args.args import Args
from args.parser import ArgsParser, AstItem
from config import Config
from logger import Logger

logger = Logger()

def surround(list: List[str], prefix: str, suffix: str) -> str:
    return prefix + f'{suffix}, {prefix}'.join(list) + suffix

def execute():
    try:
        tools = surround(['axe', 'hoe', 'pickaxe', 'shovel'], '[white bold]', '[/]')
        levels = surround(['woold', 'stone', 'iron', 'diamond', 'netherite'], '[white bold]', '[/]')

        builder = ArgsBuilder()
        builder.command('make:block', help='Create the required files to register a new block in Fabric')\
                .argument('block_id', help='The id used to reference the block throughout the code')\
                .flag('-s', '--simple', help='Do not create a custom block class')\
                .flag('-r', '--no-recipe', help='Do not create a recipe to craft this block')\
                .option('-n', '--name', help='The english name of the block for translation')\
                .option('-t', '--tool', help=f'The tool ({tools}) required to break the block')\
                .option('-l', '--level', help=f'The tool level ({levels}) required to break the block')\
            .command('make:item', help='Creates the files to register a new item')\
                .argument('item_id')\
                .flag('-s', '--simple', help='Do not create a custom item class')\
                .option('-n', '--name', help='The english name of the item for translation')

        args = builder.parse()

        if args is None:
            print('[red bold]ERROR[/]: ArgsParser has failed!')
            exit(1)

        execute_with_args(args, builder)
    except BaseException as e:
        if str(e) == '0': exit(0)
        elif str(e) == '1': exit(1)
        else:
            print('[red bold]ERROR[/]', e)
            exit(1)

def execute_with_args(args: Dict[str, AstItem | str], builder):
    root = find_root()
    if root:
        config = Config(root)
    else:
        logger.error('You need to be in a Fabric Mod project!')
        exit(1)

    match args.get('command'):
        case 'make:block':
            logger.info(f'Found mod [bold yellow]{config.mod_name}[reset] in [blue]"{root}"')
            make_block.run(Args(args.get('make:block')), config)
        case 'make:item':
            logger.info(f'Found mod [bold yellow]{config.mod_name}[reset] in [blue]"{root}"')
            make_item.run(Args(args.get('make:item')), config)
        case '':
            ArgsParser(builder).stopAndPrintUsage()
        case _:
            print(f'Action [bold underline]{args.command}[/] not implemented!')

if __name__ == '__main__':
    execute()