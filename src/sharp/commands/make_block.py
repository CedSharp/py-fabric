from argparse import Namespace

from os import path
from args.args import Args
from config import Config
from utils import make_id, make_name
from logger import Logger
from templates.block_registry import BlockRegistry
from templates.item_registry import ItemRegistry

logger = Logger('make:block')

def run(args: Args, config: Config):
    id = make_id(args.get_argument('block_id'))
    name = args.get_option('--name') or make_name(id)

    block_registry = BlockRegistry(config)
    item_registry = ItemRegistry(config)

    block_registry.add_block(id)
    item_registry.add_block_item(id)