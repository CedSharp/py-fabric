from __future__ import annotations
import sys
import os.path
from typing import Dict
from rich.console import Console
from args.command import Command
from args.option import Option, OptionType

console = Console()

class ArgsBuilder:
    def __init__(self):
        self.commands = []
        self.options = []
        self.flag('-h', '--help', help='Show this help message')

    def parse(self) -> Dict:
        from .parser import ArgsParser
        return ArgsParser(self).parse(sys.argv)

    def flag(self, *names, **kwargs) -> Command:
        self.options.append(Option(*names, parent=self, type=OptionType.FLAG, **kwargs))
        return self

    def option(self, *names, **kwargs) -> Command:
        self.options.append(Option(*names, parent=self, type=OptionType.STRING, **kwargs))
        return self

    def command(self, name: str, *args, **kwargs) -> Command:
        cmd = Command(self, name, *args, **kwargs)
        self.commands.append(cmd)
        return cmd

    def _generate_usage(self) -> str:
        app_name = os.path.basename(sys.argv[0])
        usage = f'[yellow]{app_name}[/]'
        for option in self.options:
            usage += ' ' + option.get_usage()
        if len(self.commands) > 0: usage += ' <[yellow]command[/]>'
        self.usage = usage
        return usage

    def __str__(self) -> str:
        return self._generate_usage()

    def __repl__(self) -> str:
        return self.__str__()