import re
from enum import Enum
from typing import Optional
from rich import print

R_SIMPLE_FLAG = re.compile(r'\-(?!\-)(\w|\-)')
R_NAMED_FLAG = re.compile(r'\-{2}(?!\-)((?:\w|\-)+)')

# R_SIMPLE_FLAG = re.compile(r'^\-([a-zA-Z0-1]+)(?:=(.+))?')
# R_NAMED_FLAG = re.compile(r'^\-\-([a-zA-Z0-1][a-zA-Z0-1\-_]*)(?:=(.+))?')

class OptionType(Enum):
    FLAG=0
    STRING=1
    NUMBER=2

class Option:
    def __init__(
        self,
        *names,
        parent,
        help: str = None,
        usage: str = None,
        type: OptionType = OptionType.FLAG,
        optional: bool = True,
        value_name: str = None,
    ):
        from .builder import ArgsBuilder
        from .command import Command
        self.command: ArgsBuilder | Command = parent
        self.short_name = self._get_simple_flag(*names)
        self.long_name = self._get_named_flag(*names)
        self.help = help or ''
        self.value_type = type
        self.value_name = value_name or self._get_value_name()
        self.usage = usage
        self.optional = optional

    def _get_simple_flag(*names) -> Optional[str]:
        for name in names:
            if type(name) == str:
                match = R_SIMPLE_FLAG.match(name)
                if match is not None:
                    return match.group(0)
        return None

    def _get_named_flag(*names) -> Optional[str]:
        for name in names:
            if type(name) == str:
                match = R_NAMED_FLAG.match(name)
                if match is not None:
                    return match.group(0)
        return None

    def _get_value_name(self) -> str | None:
        if self.value_type == OptionType.FLAG: return None
        if self.long_name:
            return self.long_name.replace('--', '').upper()
        return 'VALUE'

    def get_usage(self, both=False) -> str:
        return getattr(self, 'usage', self._generate_usage(both))

    def _generate_usage(self, both=False) -> str:
        usage = '[' if self.optional else ''
        if both:
            if self.short_name:
                usage += self.short_name
            if self.long_name:
                if self.short_name: usage += ',' + self.long_name
                else: usage += self.long_name
        else:
            usage += self.long_name if self.long_name else self.short_name

        if self.value_type != OptionType.FLAG:
            usage += ' [white bold]' + self.value_name + '[/]'

        usage += ']' if self.optional else ''
        self.usage = usage
        return usage

    def __str__(self) -> str:
        return self.__repl__()

    def __repl__(self) -> str:
        return self.get_usage()