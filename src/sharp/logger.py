from rich.console import Console

class Logger:
    DEBUG = False

    def __init__(self, name=None):
        self.name: str | None = name
        self.console = Console()

    def prefix(self, text: str, color: str, bold=True) -> str:
        bold_text = ' bold' if bold else ''
        name = f' [dim white]{self.name}[reset]:' if self.name else ':'
        return f'[{color}{bold_text}]{text}[reset]{name}'

    def info(self, *args, **kwargs):
        self.console.log(self.prefix('INFO', 'blue'), *args, _stack_offset=2, **kwargs)

    def warn(self, *args, **kwargs):
        self.console.log(self.prefix('WARN', 'yellow'), *args, _stack_offset=2, **kwargs)

    def error(self, *args, **kwargs):
        self.console.log(self.prefix('ERROR', 'red'), *args, _stack_offset=2, **kwargs)