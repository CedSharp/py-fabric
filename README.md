# Py Fabric

A simple library to help generate content in a Fabric mod.
It was built against minecraft version 1.18.1.

## Usage

You need to be inside of a Fabric mod project. This library was built with
a project structure based on the fabric-example-mod. Any other structure might
not work.

Once you are inside a Fabric mod project, you can see available commands by
running the script with the help flag:

```shell
py-fabric -h
```

You can also see the help for each individual commands by adding the same flag
after the command:

```shell
py-fabric make:block -h
```
