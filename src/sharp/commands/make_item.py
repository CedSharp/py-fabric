from rich import print
from args.args import Args
from config import Config
from utils import make_id, make_name
from templates.item_registry import ItemRegistry

def run(args: Args, config: Config):
    item_id = make_id(args.get_argument('item_id'))
    item_name = args.get_option('--name') or make_name(item_id)

    registry = ItemRegistry(config)
    registry.add_item(item_id)