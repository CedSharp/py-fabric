import os
from os import path
from config import Config
from logger import Logger

base_logger = Logger('Registry')

class Registry:
    def __init__(self, path: str, config: Config, logger: Logger = base_logger, template: str = ''):
        self.path = path
        self.config = config
        self.logger = logger
        self.template = template

    def process_template(template: str) -> str:
        return template

    def ensure_exists(self):
        filepath = self.path
        filedir = path.dirname(filepath)
        package_root = path.dirname(filedir)

        if package_root != self.config.package_path:
            self.logger.error('Trying to create', self.path, 'in a non-project!')
            exit(1)

        if not path.exists(filepath):
            if not path.exists(filedir):
                rel = path.relpath(filedir, self.config.package_path)
                self.logger.info(f'Created directory [blue]{rel}')
                os.mkdir(filedir)

            with open(filepath, 'x') as f:
                template = self.process_template(self.template)
                f.write(template)

            self.after_creating_file(filepath)
    
    def after_creating_file(self, file: str):
        rel = path.relpath(file, self.config.package_path)
        self.logger.info(f'Created [blue]{rel}')
