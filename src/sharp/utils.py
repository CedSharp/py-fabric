import os
from os import path
from typing import Optional

def make_id(text: str) -> str:
    return text.lower().replace('-', '_').replace(' ', '_')

def make_name(id: str) -> str:
    return ' '.join(map(lambda x: x.capitalize(), id.replace('_', ' ').split(' ')))

def find_parent_with_file(filename: str) -> Optional[str]:
    dir = os.getcwd()
    while True:
        files = os.listdir(dir)
        if filename in files:
            return dir
        parent_dir = path.dirname(dir)
        if parent_dir == dir:
            return None
        dir = parent_dir

def find_root() -> Optional[str]:
    return find_parent_with_file('build.gradle')