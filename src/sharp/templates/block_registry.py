import os
from os import path
from typing import List
from rich.syntax import Syntax
from templates.registry import Registry
from templates.item_registry import ItemRegistry
from templates.item_group_registry import ItemGroupRegistry

from config import Config
from logger import Logger

logger = Logger('BlockRegistry')

template = """package [[PACKAGE]].registry;

import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;

import [[PACKAGE]].[[ENTRY_CLASS]];

public class [[ENTRY_CLASS]]Blocks {

    public static void registerAllBlocks() {
        [[ENTRY_CLASS]].LOGGER.info("Registered all blocks!");
    }

    private static Block registerBlock(String name, Block block) {
        Identifier block_id = new Identifier([[ENTRY_CLASS]].MODID, name);
        [[ENTRY_CLASS]].LOGGER.debug("- Register block " + block_id);
        return Registry.register(Registry.BLOCK, block_id, block);
    }
}
"""

block_def = """\n\tpublic static final Block [[DEF_NAME]] =
\t\tregisterBlock("[[BLOCK_NAME]]", new Block(FabricBlockSettings
\t\t\t.of(Material.STONE)
\t\t\t.sounds(BlockSoundGroup.STONE)
\t\t\t.hardness(4f)
\t\t\t.strength(1f)
\t\t\t.requiresTool()));\n"""

class BlockRegistry(Registry):
    def __init__(self, config: Config):
        filepath = config.get_block_registry()
        super().__init__(filepath, config, logger, template)

    def process_template(self, template: str) -> str:
        template_package = self.config.package
        entry_class = self.config.entry_filename

        return template\
            .replace('[[PACKAGE]]', template_package)\
            .replace('[[ENTRY_CLASS]]', entry_class)

    def after_creating_file(self, file: str):
        cls_name = self.config.entry_filename + 'Items'
        rel = path.relpath(file, self.config.package_path)
        logger.info(f'Created class [white bold]{cls_name}[reset] in [blue]{rel}')
    
    def add_block(self, id: str):
        self.ensure_exists()

        with open(self.path, 'r') as f:
            content = f.readlines()

        insertDefIndex = self._get_index_to_insert_block_def(content)
        content.insert(insertDefIndex, block_def
            .replace('[[DEF_NAME]]', id.upper())
            .replace('[[BLOCK_NAME]]', id)
        )

        with open(self.path, 'w') as f:
            f.write(''.join(content))

        rel = path.relpath(self.path, self.config.package_path)
        logger.info(f'Added [white bold]{id.upper()}[reset] to [blue]{rel}')

    def _get_index_to_insert_block_def(self, content: List[str]) -> int:
        registryClass = self.config.entry_filename + 'Blocks'

        for index, line in enumerate(content):
            if line.startswith(f'public class {registryClass} {{'):
                return index + 1

        logger.error('File content doesn\'t seem valid!')
        exit(1)
