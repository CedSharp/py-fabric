from typing import Dict, List, Tuple, TypedDict
from rich import box
from rich.console import Console
from rich.padding import Padding
from rich.table import Table

from args.option import OptionType
from args.builder import ArgsBuilder
from args.option import Option
from logger import Logger

console = Console()
logger = Logger('ArgsParser')

class AstOption(TypedDict):
    option: Option
    value: bool | str

class AstArgument(TypedDict):
    name: str
    value: str

class AstItem(TypedDict):
    options: List[AstOption]
    arguments: List[AstArgument]

class ArgsParser:
    def __init__(self, builder: ArgsBuilder):
        self.builder = builder
        self.command = builder
        self.args = []
        self.ast: Dict[str, AstItem|str] = {
            'command': '',
            '': {'options': [], 'arguments': []}
        }

    def eof(self) -> bool:
        return len(self.args) == 0

    def peek(self) -> str:
        return self.args[0]

    def next(self) -> str:
        return self.args.pop(0)

    def parse(self, argv: List[str]) -> Dict[str, AstItem]:
        self.args = argv.copy()
        if len(self.args) > 0:
            self.args.pop(0)

        while self.eof() == False:
            size = len(self.args)

            if self.peek().startswith('--'):
                self.parseNamedOption()
            elif self.peek().startswith('-'):
                self.parseSimpleOptions()
            else:
                self.parseCommand()

            if size == len(self.args):
                raise BaseException('Nothing happenned, unexpected error!')

        self.validate()
        return self.ast

    def validate(self):
        if type(self.command) == ArgsBuilder: return
        target = self.ast.get(self.command.name)

        # Validate all arguments were provided
        arg_count = len(target.get('arguments'))
        if arg_count < len(self.command.arguments):
            args = ', '.join(map(lambda x: x.get('name'), self.command.arguments[arg_count:]))
            raise BaseException('Missing required arguments: '+args)

        # Validate all required options were provided
        options = map(lambda x: x.get('option'), target.get('options'))
        for o in self.command.options:
            if o.optional == False and o not in options:
                raise BaseException('Missing required option: ' +o.long_name)

    def stopAndPrintUsage(self):
        console.print('Usage:')
        console.print('    ' + self.command.__str__())
        min_column_width = 20

        if type(self.command) == ArgsBuilder:
            cmds = Table(show_header=False, box=None)
            cmds.add_column(no_wrap=True, min_width=min_column_width)
            cmds.add_column()

            console.print('\nCommands:')
            for c in self.command.commands:
                cmds.add_row(
                    Padding(f'[yellow]{c.name}', (0, 2, 0, 2)),
                    c.help
                )

            console.print(cmds)
        elif len(self.command.arguments) > 0:
            args = Table(show_header=False, box=None)
            args.add_column(no_wrap=True, min_width=min_column_width)
            args.add_column()

            console.print('\nArguments:')
            for arg in self.command.arguments:
                args.add_row(
                    Padding(arg['name'], (0, 2)),
                    arg['help']
                )

            console.print(args)

        options = Table(show_header=False, box=None)
        options.add_column(no_wrap=True, min_width=min_column_width)
        options.add_column()
    
        console.print('\nOptions:')
        for o in self.command.options:
            usage = o.get_usage(True)
            if o.optional: usage = usage[1:len(usage)-1]
            options.add_row(
                Padding(usage, (0, 2, 0, 2)),
                o.help
            )

        console.print(options)
        console.print('')
        exit(0)

    def isTruthy(self, value: str | None) -> bool | None:
        if value is None: return True
        match value.lower():
            case '1': return True
            case 'y': return True
            case 'yes': return True
            case 'on': return True
            case 'true': return True

            case '0': return False
            case 'n': return False
            case 'no': return False
            case 'off': return False
            case 'false': return False

            case _: return None

    def parseEqualValue(self) -> Tuple[str, str]:
        val = self.next()
        try:
            index = val.index('=')
            return (val[0:index], val[index+1:])
        except ValueError:
            return (val, None)

    def parseNamedOption(self):
        option_name, value = self.parseEqualValue()
        option_name = option_name[2:]

        if option_name == 'help':
            self.stopAndPrintUsage()

        option: Option = next(o for o in self.command.options\
                                      if o.long_name == '--' + option_name)
        t_value = self.isTruthy(value)

        if option.value_type == OptionType.FLAG and t_value == None:
            raise BaseException(f'Invalid value for boolean flag --{option_name}')
        elif option.value_type != OptionType.FLAG and value == None:
            if self.eof() or (not self.eof() and self.peek().startswith('-')):
                raise BaseException(f'Option --{option_name} requires a value')
            else: value = self.next()
        
        target = self.ast.get('') if type(self.command) == ArgsBuilder\
            else self.ast.get(self.command.name)
        target.get('options').append({
            'option': option,
            'value': t_value if option.value_type == OptionType.FLAG else value
        })

    def parseSimpleOptions(self):
        options, value = self.parseEqualValue()
        options = options[1:]
        size = len(options)
        for i, o in enumerate(options):
            if i + 1 == size:
                try:
                    self.parseSimpleOption(o, value)
                except BaseException as e:
                    if self.eof(): raise e
                    else:
                        val = self.next()
                        if val.startswith('-'): raise e
                        else: self.parseSimpleOption(o, val)
            else:
                self.parseSimpleOption(o)

    def parseSimpleOption(self, name: str, value: str = None):
        if name == 'h':
            self.stopAndPrintUsage()

        try:
            option: Option = next(o for o in self.command.options\
                                    if o.short_name == '-' + name)
            t_value = self.isTruthy(value)

            if option.value_type == OptionType.FLAG and t_value == None:
                raise BaseException(f'Invalid value for boolean flag -{name}')
            elif option.value_type != OptionType.FLAG and value == None:
                raise BaseException(f'Option -{name} requires a value')
            
            target = self.ast.get('') if type(self.command) == ArgsBuilder\
                else self.ast.get(self.command.name)
            target.get('options').append({
                'option': option,
                'value': t_value if option.value_type == OptionType.FLAG else value
            })
        except StopIteration:
            logger.error(f'Invalid option -{name}')
            exit(1)

    def parseCommand(self):
        cmd_name = self.next()
        try:
            cmd = next(cmd for cmd in self.builder.commands if cmd.name == cmd_name)
            if type(self.command) != ArgsBuilder:
                raise BaseException(f'Too many commands, expected only one')
            self.ast[cmd_name] = {'options': [], 'arguments': []}
            self.ast['command'] = cmd_name
            self.command = cmd
        except StopIteration:
            if type(self.command) != ArgsBuilder:
                args = self.ast.get(self.command.name).get('arguments')
                index = len(args)
                if index < len(self.command.arguments):
                    args.append({
                        'name': self.command.arguments[index]['name'],
                        'value': cmd_name
                    })
                else: raise BaseException(f'Not a valid argument: "{cmd_name}"')
            else: raise BaseException(f'Not a valid command: "{cmd_name}"')
