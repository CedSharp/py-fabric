class Args:
    def __init__(self, args: dict):
        self.args = args

    def get_argument(self, name: str) -> str | None:
        try:
            return next(a.get('value') for a in self.args.get('arguments') if a.get('name') == name)
        except StopIteration:
            return None

    def get_option(self, name: str) -> str | None:
        try:
            if len(name) == 2:
                return next(o.get('value') for o in self.args.get('options') if o.get('option').short_name == name)
            else:
                return next(o.get('value') for o in self.args.get('options') if o.get('option').long_name == name)
        except StopIteration:
            return None